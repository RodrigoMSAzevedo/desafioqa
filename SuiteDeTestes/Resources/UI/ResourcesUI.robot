*** Settings ***
Library         Selenium2Library

*** Variables ***
${Url_Unimed}       https://www.unimed.coop.br/
${Browser}          Firefox
${SELENIUM_HUB}     http://selenium_hub:4444/wd/hub 

*** Keywords ***
Validar que a cidade nao aparece
    [Arguments]  ${cidade}  ${pagina}
    Set Selenium Timeout                5
    Page Should Not Contain Element     xpath=//p[contains(., '${cidade}')]
    Set Focus To Element                css=nav[id="menuAcessoRapido"]
    sleep                               3
    Click Element                       css=li a[class="proximo"]

Dado que tenha acessado a pagina da Unimed
    Set Selenium Implicit Wait      5
    Open Browser  ${Url_Unimed}  ${Browser}  remote_url=${SELENIUM_HUB}

Quando acesso a opcao de Guia Medico
    sleep           2
    Click Element   css=a[href="/guia-medico"] h2

E faco uma pesquisa por Cirurgia Geral
    Input Text      css=input[id="campo_pesquisa"]      Cirurgia Geral
    Click Element   css=button[id="btn_pesquisar"]
    Click Element   css=div[class="s-field control-group selecione-rede big-field pesquisa-avancada"] div[class="css-k71zgk"]
    Click Element   id:react-select-2-option-18
    Click Element   css=div[class="s-field control-group selecione-plano big-field pesquisa-avancada"] div[class="css-k71zgk"]
    Click Element   id:react-select-3-option-67
    Click Element   css=input[value="37"]
    Click Button    css=button[class="btn btn-success"]

Entao visualizo os resultados da pesquisa na pagina com sucesso
    Page Should Contain     Especialidade(s): Cirurgia Geral
    Page Should Contain     Rio de Janeiro

Entao visualizo que nas paginas 1, 2 e 3 a cidade de Sao Paulo nao aparece nos resultados da pesquisa
    Validar que a cidade nao aparece  São Paulo  1
    Validar que a cidade nao aparece  São Paulo  2
    Validar que a cidade nao aparece  São Paulo  3