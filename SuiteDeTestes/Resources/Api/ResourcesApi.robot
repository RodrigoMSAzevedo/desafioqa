*** Settings ***
Library  RequestsLibrary
Library  Collections

*** Variables ***
${ID_DO_FILME}  tt1285016
${API_KEY}      52ec71bf

*** Keywords ***
Realizar resquisicao
    Create Session      sessao              http://www.omdbapi.com/
    ${headers}          create dictionary   content-type=application/json
    Set To Dictionary                   ${headers}
    ${resp}         Get Request     sessao    ?i=${ID_DO_FILME}&apikey=${API_KEY}  headers=${headers}
    ${output}=    To Json    ${resp.content}    pretty_print=True
    set test variable   ${output}

Validar Titulo, ano e idioma do filme
    should contain      ${output}     "Title": "The Social Network"
    should contain      ${output}     "Year": "2010"
    should contain      ${output}     "Language": "English, French"

Validar titulo de filme inexistente
    should not contain  ${output}     "Title": "The Lord of the Rings"