*** Settings ***
Resource    ../../Resources/UI/ResourcesUI.robot
Test Teardown  Close Browser

*** Test Cases ***
Teste 1
    Dado que tenha acessado a pagina da Unimed
    Quando acesso a opcao de Guia Medico
    E faco uma pesquisa por Cirurgia Geral
    Entao visualizo os resultados da pesquisa na pagina com sucesso

Teste 2
    Dado que tenha acessado a pagina da Unimed
    Quando acesso a opcao de Guia Medico
    E faco uma pesquisa por Cirurgia Geral
    Entao visualizo que nas paginas 1, 2 e 3 a cidade de Sao Paulo nao aparece nos resultados da pesquisa